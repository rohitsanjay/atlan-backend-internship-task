## Commands for running server
VERSION := 0.0.1

.PHONY: build
build:
	docker build -t atlan-collect-api-server:${VERSION} . -f ./Dockerfile

.PHONY: dev
dev: build
	docker run -p 5000:8080 --rm -d \
	--env-file secrets/.env.docker \
	-v `pwd`/instance:/code/atlan_collect_api/instance \
	--name atlan-collect-api-server atlan-collect-api-server:${VERSION}

.PHONY: db-init
db-init:
	docker exec atlan-collect-api-server atlan_collect_api db init

.PHONY: db-upgrade
db-upgrade:
	docker exec atlan-collect-api-server atlan_collect_api db upgrade

.PHONY: db-migrate
db-migrate:
	docker exec atlan-collect-api-server atlan_collect_api db migrate

.PHONY: db-downgrade
db-downgrade:
	docker exec atlan-collect-api-server atlan_collect_api db downgrade

.PHONY: init
init:
	docker exec atlan-collect-api-server atlan_collect_api init
