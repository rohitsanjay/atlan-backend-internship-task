#!/bin/bash
echo "Starting server..."

if [ "$FLASK_ENV" == "development" ]; then
    cd /code/atlan_collect_api
    export FLASK_APP=atlan_collect_api.wsgi
    flask run -h 0.0.0.0 -p 8080 >>/var/log/evalserver.log 2>&1
else
    gunicorn --chdir /code/atlan_collect_api atlan_collect_api.wsgi:app -b '0.0.0.0:8080' >>/var/log/apiserver.log 2>&1
fi
