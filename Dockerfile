FROM python:3.7.9

RUN mkdir /code
WORKDIR /code/atlan_collect_api

COPY requirements.txt setup.py tox.ini ./
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install -e .

ADD atlan_collect_api atlan_collect_api
ADD migrations migrations
ADD services/apiserver.sh /opt/run.sh

ADD . .

ENV FLASK_ENV production
ENV PYTHONUNBUFFERED 0
ENTRYPOINT [ "/opt/run.sh" ]
