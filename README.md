# Atlan Backend Internship Task

## Demo Video

https://youtu.be/8R_uWUn1duI

## Problem

Implementation through which a user can stop a long-running task at any given point in time, and can choose to resume or terminate it.

## Tech Stack Used

1. [Flask][flask] for API server
2. [SQLite][sqlite] for database

## Instructions to run locally

#### 1. Clone the repository and change directory

```bash
git clone https://github.com/rohitsanj/atlan-backend-internship-task.git
# or
git clone https://rohitsanjay@bitbucket.org/rohitsanjay/atlan-backend-internship-task.git

cd atlan-backend-internship-task
```

#### 2. Create a directory called `instance` and download the following files into it:

- [sales_data.csv][sales-data-csv]
- [upload_sales_data.csv][upload-sales-data-csv]

The directory must look like:

```
instance/
   sales_data.csv
   upload_sales_data.csv
```

#### 3. Create a directory called `secrets` and add the following text into a file called `.env.docker`:

```bash
# secrets/.env.docker
FLASK_ENV=production
SQLALCHEMY_DATABASE_URI=sqlite:////code/atlan_collect_api/instance/atlan_collect_api.db

SAMPLE_CSV_DATA_PATH=/code/atlan_collect_api/instance/sales_data.csv
EXPORT_CSV_DATA_DIR=/code/atlan_collect_api/instance
SIMULATION_TIME=0.01
```

#### 4. Build image and run container.

```
make dev
```

#### 5. Navigate to http://127.0.0.1:5000/ and you will see "Hello World!"

## Database Commands

### Database upgrade

You will need to run this to create the database instance, which will be located in the `instance` directory created.

```
make db-upgrade
```

### Database migration

```
make db-migrate
```

### Database initialization

This is used to initialize the database with sample data. It is to be run at the beginning, and must be run only once.

```
make init
```

## Endpoints

The API endpoints have been documented and published using Postman. You can find it [here](https://documenter.getpostman.com/view/6587831/TVemCVpB).

[flask]: https://flask.palletsprojects.com/en/1.1.x/
[sqlite]: https://www.sqlite.org/index.html
[sales-data-csv]: https://drive.google.com/file/d/1qw3s1pxCInFp5JdvLMgJGG9fvhiJyBiY/view?usp=sharing
[upload-sales-data-csv]: https://drive.google.com/file/d/1y0S0PMby8yS06KtrEG2n3IRDR4OCplrf/view?usp=sharing

## Possible Improvements

- Create user table and keep track of user jobs in a centralized cache such as Redis, so that multiple workers can be spawned instead of a single worker.
- User authentication on every endpoint
