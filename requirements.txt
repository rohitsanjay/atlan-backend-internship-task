flask
flask-migrate
flask-sqlalchemy
python-dotenv
tox
gunicorn
