import os
import csv
import time

from flask import current_app

from atlan_collect_api.tasks.task import Task
from atlan_collect_api.extensions import db
from atlan_collect_api.models import Sales
from atlan_collect_api.utils import convert_to_datetime
from atlan_collect_api.tasks.exceptions import TaskError


class DownloadTask(Task):
    def __init__(self, start_date, end_date):
        super().__init__()
        self.start_date = start_date
        self.end_date = end_date
        self.start_date_datetime = convert_to_datetime(start_date)
        self.end_date_datetime = convert_to_datetime(end_date)

        self.row_indices = None

        self.export_filepath = os.path.join(
            current_app.config.get("EXPORT_CSV_DATA_DIR"),
            "export-{}-{}.csv".format(self.start_date, self.end_date)
        )

    def start(self):
        self._validate_halted_task()

        if self.row_indices is None:
            self.row_indices = [x[0] for x in db.session.query(Sales.id).filter(
                Sales.date.between(self.start_date_datetime, self.end_date_datetime)
            )]
            self.num_rows = len(self.row_indices)
            if not self.row_indices:
                return

        if self.status_index == 0 and os.path.exists(self.export_filepath):
            os.remove(self.export_filepath)

        with open(self.export_filepath, "a") as f:
            dict_writer = csv.DictWriter(f, fieldnames=Sales.fieldnames())
            if self.status_index == 0:
                dict_writer.writeheader()

            for row_index in self.row_indices[self.status_index:]:
                if self.paused or self.terminated:
                    break

                row = Sales.query.filter_by(id=row_index).one().serialize

                # Simulate long running task
                time.sleep(current_app.config.get("SIMULATION_TIME"))

                dict_writer.writerow(row)
                self.status_index += 1

        if self.status_index == self.num_rows:
            self.completed = True

    def terminate(self):
        if self.terminated is False:
            # Rollback work done
            os.remove(self.export_filepath)
            self.terminated = True
        else:
            raise TaskError("Task has already been terminated.")

    @property
    def progress(self):
        if hasattr(self, "num_rows"):
            return (self.status_index / self.num_rows) * 100

        return 0
