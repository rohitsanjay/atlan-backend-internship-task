import io
import csv
import time

from flask import current_app

from atlan_collect_api.tasks.task import Task
from atlan_collect_api.extensions import db
from atlan_collect_api.models import Sales
from atlan_collect_api.tasks.exceptions import TaskError
from atlan_collect_api.utils import create_sales_data_row_dict


class UploadTask(Task):
    def __init__(self, filestream):
        super().__init__()
        self.filestring = filestream.read().decode("utf-8")

        self.data = []
        self.rows = None

    def start(self):
        self._validate_halted_task()

        if self.rows is None:
            self.rows = list(csv.DictReader(io.StringIO(self.filestring)))
            self.num_rows = len(self.rows)

        for row in self.rows[self.status_index:]:

            if self.paused or self.terminated:
                break

            self.data.append(Sales(**create_sales_data_row_dict(row)))

            # Simulate long running task
            time.sleep(current_app.config.get("SIMULATION_TIME"))

            self.status_index += 1

        if self.status_index == self.num_rows:
            # Add and commit the work only when task is completed
            db.session.add_all(self.data)
            db.session.commit()
            self.completed = True

    def terminate(self):
        if self.terminated is False:
            # Rollback work done
            db.session.rollback()
            self.terminated = True
        else:
            raise TaskError("Task has already been terminated.")

    @property
    def progress(self):
        if hasattr(self, "num_rows"):
            return (self.status_index / self.num_rows) * 100

        return 0
