from atlan_collect_api.tasks.download_task import DownloadTask
from atlan_collect_api.tasks.upload_task import UploadTask

__all__ = ["DownloadTask", "UploadTask"]
