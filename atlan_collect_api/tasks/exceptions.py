class TaskError(Exception):
    """Generic exception for usage in Task classes"""
    pass
