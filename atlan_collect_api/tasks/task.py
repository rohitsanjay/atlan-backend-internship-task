from atlan_collect_api.tasks.exceptions import TaskError


class Task:
    def __init__(self):
        self.paused = False
        self.terminated = False
        self.completed = False
        self.status_index = 0

    def _validate_halted_task(self):
        if self.terminated:
            raise TaskError("Task has been terminated. Cannot resume task.")

        if self.paused:
            # Resume the task
            self.paused = False

    def start(self):
        raise NotImplementedError

    def terminate(self):
        raise NotImplementedError

    def pause(self):
        if self.paused is False:
            self.paused = True
        else:
            raise TaskError("Task has already been paused.")

    @property
    def progress(self):
        raise NotImplementedError

    def __call__(self):
        self.start()
