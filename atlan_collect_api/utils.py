from threading import Thread
from datetime import datetime

from flask import has_app_context, _app_ctx_stack, jsonify, make_response


def send_response(message: dict, status_code: int):
    return make_response(jsonify(message), status_code)


def convert_to_datetime(string):
    return datetime.strptime(string, "%Y-%m-%d")


def create_sales_data_row_dict(row):
    return {
        "date": convert_to_datetime(row['Date']),
        "year": row['Year'],
        "country": row['Country'],
        "state": row['State'],
        "product_category": row['Product Category'],
        "sub_category": row['Sub Category'],
        "quantity": row['Quantity'],
        "unit_cost": row['Unit Cost'],
        "unit_price": row['Unit Price'],
        "cost": row['Cost'],
        "revenue": row['Revenue'],
    }

# Source: https://github.com/sintezcs/flask-threads/blob/master/flaskthreads/thread_helpers.py#L178


class AppContextThread(Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not has_app_context():
            raise RuntimeError
        self.app_ctx = _app_ctx_stack.top

    def run(self):
        try:
            self.app_ctx.push()
            super().run()
        finally:
            self.app_ctx.pop()
