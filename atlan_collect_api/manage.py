import click
import csv

from flask.cli import FlaskGroup
from flask import current_app

from atlan_collect_api.app import create_app
from atlan_collect_api.utils import create_sales_data_row_dict


def create_atlan_collect_api(info):
    return create_app(cli=True)


@click.group(cls=FlaskGroup, create_app=create_atlan_collect_api)
def cli():
    """Main entry point"""


@cli.command("init")
def init():
    # Create a new admin user
    from atlan_collect_api.extensions import db
    from atlan_collect_api.models import User, Sales

    click.echo("create user")
    user = User(username="admin", email="admin@mail.com")
    db.session.add(user)
    db.session.commit()
    click.echo("created user admin")

    # Load database with sample data
    click.echo("Loading sample data")
    sample_csv_data_path = current_app.config.get("SAMPLE_CSV_DATA_PATH")

    data = []
    with open(sample_csv_data_path) as f:
        reader = csv.DictReader(f)
        for row in reader:
            data.append(Sales(**create_sales_data_row_dict(row)))

    db.session.add_all(data)
    db.session.commit()

    click.echo("Done!")


if __name__ == "__main__":
    cli()
