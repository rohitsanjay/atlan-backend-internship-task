import os

from flask import Flask
from dotenv import load_dotenv

from atlan_collect_api.extensions import db, migrate


def create_app(cli=False):
    """Application factory, used to create application"""
    app = Flask("atlan_collect_api")

    env_file = os.environ.get("ENV_FILE", default=".env")
    env_path = os.path.join("secrets", env_file)
    load_dotenv(dotenv_path=env_path)

    app.config.from_object("atlan_collect_api.config")
    app.url_map.strict_slashes = False

    configure_extensions(app, cli)
    register_blueprints(app)

    return app


def configure_extensions(app, cli):
    """configure flask extensions"""
    db.init_app(app)

    if cli is True:
        migrate.init_app(app, db)


def register_blueprints(app):
    """register all blueprints for application"""
    from atlan_collect_api.api import home
    from atlan_collect_api.api import upload
    from atlan_collect_api.api import download

    app.register_blueprint(home.bp, url_prefix="/")
    app.register_blueprint(upload.bp, url_prefix="/upload")
    app.register_blueprint(download.bp, url_prefix="/download")
