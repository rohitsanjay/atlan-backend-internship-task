"""Default configuration

Use env var to override
"""
import os

ENV = os.getenv("FLASK_ENV")
DEBUG = ENV == "development"
SECRET_KEY = os.getenv("SECRET_KEY")

SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")
SQLALCHEMY_TRACK_MODIFICATIONS = False

SAMPLE_CSV_DATA_PATH = os.getenv("SAMPLE_CSV_DATA_PATH")
EXPORT_CSV_DATA_DIR = os.getenv("EXPORT_CSV_DATA_DIR")
SIMULATION_TIME = float(os.getenv("SIMULATION_TIME", default=0.01))
