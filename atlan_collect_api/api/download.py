from functools import wraps

from flask import request, Blueprint
from atlan_collect_api.tasks.download_task import DownloadTask
from atlan_collect_api.tasks.exceptions import TaskError
from atlan_collect_api.utils import AppContextThread, send_response

bp = Blueprint("download", __name__)


download_task_instance = None


def validate_download_request(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        global download_task_instance
        if download_task_instance is None:
            return send_response(
                message={
                    "message": "Invalid request: No download task has been submitted",
                },
                status_code=400
            )
        return func(*args, **kwargs)
    return wrapper


@bp.route("/", methods=["GET"])
def download():
    global download_task_instance

    if download_task_instance is not None and not download_task_instance.completed:
        return send_response(
            message={
                "message": "Download task has already been scheduled. Wait till task finishes.",
            },
            status_code=503
        )

    start_date = request.args.get("start_date")
    end_date = request.args.get("end_date")

    if not start_date or not end_date:
        return send_response(
            message={
                "message": "Please provide 'start_date' and 'end_date' as query parameters"
            },
            status_code=400
        )

    download_task_instance = DownloadTask(start_date=start_date, end_date=end_date)

    download_thread = AppContextThread(name="Download task", target=download_task_instance)
    download_thread.start()

    return send_response(
        message={
            "message": "Download task has been submitted",
        },
        status_code=200
    )


@bp.route("/pause", methods=["POST"])
@validate_download_request
def pause():
    global download_task_instance

    try:
        download_task_instance.pause()
    except TaskError:
        return send_response(
            message={
                "message": "Download task has already been paused. You can either resume or terminate the task.",
            },
            status_code=200
        )

    return send_response(
        message={
            "message": "Download task has been paused",
        },
        status_code=200
    )


@bp.route("/terminate", methods=["POST"])
@validate_download_request
def terminate():
    global download_task_instance

    download_task_instance.terminate()

    download_task_instance = None

    return send_response(
        message={
            "message": "Download task has been terminated",
        },
        status_code=200
    )


@bp.route("/resume", methods=["POST"])
@validate_download_request
def resume():
    global download_task_instance

    if not download_task_instance.terminated:
        download_thread = AppContextThread(name="Download task", target=download_task_instance)
        download_thread.start()

        return send_response(
            message={
                "message": "Download task has been resumed",
            },
            status_code=200
        )

    return send_response(
        message={
            "message": "Download task has been terminated. Cannot resume task.",
        },
        status_code=400
    )


@bp.route("/progress", methods=["GET"])
@validate_download_request
def progress():
    global download_task_instance

    return send_response(
        message={
            "progress": download_task_instance.progress,
        },
        status_code=200
    )
