from flask import Blueprint

bp = Blueprint("/", __name__)


@bp.route("/", methods=["GET"])
def home():
    return "Hello World!"
