from functools import wraps

from flask import request, Blueprint
from atlan_collect_api.tasks.upload_task import UploadTask
from atlan_collect_api.tasks.exceptions import TaskError
from atlan_collect_api.utils import AppContextThread, send_response

bp = Blueprint("upload", __name__)


upload_task_instance = None


def validate_upload_request(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        global upload_task_instance
        if upload_task_instance is None:
            return send_response(
                message={
                    "message": "Invalid request: No upload task has been submitted",
                },
                status_code=400
            )
        return func(*args, **kwargs)
    return wrapper


@bp.route("/", methods=["POST"])
def upload():
    global upload_task_instance

    if upload_task_instance is not None and not upload_task_instance.completed:
        return send_response(
            message={
                "message": "Upload task has already been scheduled. Wait till task finishes.",
            },
            status_code=503
        )

    uploaded_file = request.files.get("data")

    upload_task_instance = UploadTask(uploaded_file)

    upload_thread = AppContextThread(name="upload task", target=upload_task_instance)
    upload_thread.start()

    return send_response(
        message={
            "message": "Upload task has been submitted",
        },
        status_code=200
    )


@bp.route("/pause", methods=["POST"])
@validate_upload_request
def pause():
    global upload_task_instance

    try:
        upload_task_instance.pause()
    except TaskError:
        return send_response(
            message={
                "message": "Upload task has already been paused. You can either resume or terminate the task.",
            },
            status_code=400
        )

    return send_response(
        message={
            "message": "Upload task has been paused",
        },
        status_code=200
    )


@bp.route("/terminate", methods=["POST"])
@validate_upload_request
def terminate():
    global upload_task_instance

    upload_task_instance.terminate()

    upload_task_instance = None

    return send_response(
        message={
            "message": "Upload task has been terminated",
        },
        status_code=200
    )


@bp.route("/resume", methods=["POST"])
@validate_upload_request
def resume():
    global upload_task_instance

    if not upload_task_instance.terminated:
        upload_thread = AppContextThread(name="Upload task", target=upload_task_instance)
        upload_thread.start()

        return send_response(
            message={
                "message": "Upload task has been resumed",
            },
            status_code=200
        )

    return send_response(
        message={
            "message": "Upload task has been terminated. Cannot resume task.",
        },
        status_code=400
    )


@bp.route("/progress", methods=["GET"])
@validate_upload_request
def progress():
    global upload_task_instance

    return send_response(
        message={
            "progress": upload_task_instance.progress,
        },
        status_code=200
    )
