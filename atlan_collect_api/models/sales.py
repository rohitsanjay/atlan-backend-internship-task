import datetime

from atlan_collect_api.extensions import db


class Sales(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    year = db.Column(db.Integer, nullable=False)
    country = db.Column(db.String(80), nullable=False)
    state = db.Column(db.String(80), nullable=False)
    product_category = db.Column(db.String(80), nullable=False)
    sub_category = db.Column(db.String(80), nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    unit_cost = db.Column(db.Float, nullable=False)
    unit_price = db.Column(db.Float, nullable=False)
    cost = db.Column(db.Float, nullable=False)
    revenue = db.Column(db.Float, nullable=False)

    @property
    def serialize(self):
        return {
            "date": self.date,
            "year": self.year,
            "country": self.country,
            "state": self.state,
            "product_category": self.product_category,
            "sub_category": self.sub_category,
            "quantity": self.quantity,
            "unit_cost": self.unit_cost,
            "unit_price": self.unit_price,
            "cost": self.cost,
            "revenue": self.revenue,
        }

    @staticmethod
    def fieldnames():
        return "date,year,country,state,product_category,sub_category,quantity,unit_cost,unit_price,cost,revenue".split(
            ",")
