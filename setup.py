from setuptools import setup, find_packages

__version__ = "0.1"

setup(
    name="atlan_collect_api",
    version=__version__,
    packages=find_packages(),
    install_requires=[
        "flask",
        "flask-sqlalchemy",
        "flask-migrate",
        "python-dotenv",
    ],
    entry_points={
        "console_scripts": [
            "atlan_collect_api = atlan_collect_api.manage:cli"
        ]
    },
)
